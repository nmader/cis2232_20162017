package info.hccis.admin.web;

import info.hccis.admin.dao.UserAccessDAO;
import info.hccis.admin.data.springdatajpa.CodeTypeRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.entity.Users;
import info.hccis.admin.model.jpa.CodeType;
import info.hccis.admin.web.services.UserAccess;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author knewcombe
 */
@Controller
public class UsersController {

    @RequestMapping("/users/users")
    public String showUsers(Model model, HttpSession session) {
        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        ArrayList<Users> users = UserAccessDAO.getUsers(databaseConnection);
        model.addAttribute("users", users);
        return "users/users";
    }

    @RequestMapping("/users/addUser")
    public String addUser(Model model, HttpSession session, @ModelAttribute("user") Users user) {
        UserAccess userAccess = new UserAccess();
        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        if (!"".equals(user.getUsername())) {
            //Setting Hashed password
            user.setPassword(userAccess.getHashPassword(user.getPassword()));
            String result = userAccess.addUserToDatabase(user, databaseConnection);
            if (!result.equals("Fail")) {
                model.addAttribute("message", "User added");
                model.addAttribute("user", new Users());
                ArrayList<Users> users = UserAccessDAO.getUsers(databaseConnection);
                model.addAttribute("users", users);
                return "users/users";
            } else {
                model.addAttribute("message", "Failed to add user");
                model.addAttribute("user", user);
            }
        }
        return "users/addUser";

    }

//    @RequestMapping("/users/editUser")
//    public String editUser(Model model, HttpSession session, HttpServletRequest request){
//        UserAccess getUser = new UserAccess();
//        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
//        System.out.println(request.getParameter("id"));
//        Users selectUser = getUser.selectUser(Integer.parseInt(request.getParameter("id")), databaseConnection);
//        System.out.println(selectUser.getUserAccessId());
//        model.addAttribute("user", selectUser);
//        return "users/editUser";
//    }
//    @RequestMapping("/users/editUsers")
//    public String edit(Model model, HttpSession session, HttpServletRequest request){
//        UserAccess edit = new UserAccess();
//        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
//        Users editUser = new Users();
//        editUser.setUserAccessId(Integer.parseInt(request.getParameter("id")));
//        editUser.setUsername(request.getParameter("username"));
//        editUser.setPassword(request.getParameter("password"));
//        editUser.setUserTypeCode(Integer.parseInt(request.getParameter("userTypeCode")));
//        
//        edit.updateUser(editUser, databaseConnection);
//        return "users/users";
//    }
    @RequestMapping("/users/deleteUser")
    public String deleteUser(Model model, HttpSession session, HttpServletRequest request) {
        UserAccess deleteUser = new UserAccess();
        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        deleteUser.removeUser(Integer.parseInt(request.getParameter("id")), databaseConnection);
        ArrayList<Users> users = UserAccessDAO.getUsers(databaseConnection);
        model.addAttribute("users", users);
        return "users/users";
    }

}
