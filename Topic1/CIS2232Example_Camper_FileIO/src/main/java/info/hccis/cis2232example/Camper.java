package info.hccis.cis2232example;

import java.util.Scanner;

/**
 * This class will represent a camper.
 *
 * @author bjmaclean
 * @since 20150915
 */
public class Camper {

    private static int maxRegistrationId;
    private int registrationId;
    private String firstName;
    private String lastName;
    private String dob;

    /**
     * Default constructor which will get info from user
     *
     * @since 20150917
     * @author BJ MacLean
     */
    public Camper() {
        System.out.println("Enter first name");
        this.firstName = FileUtility.getInput().nextLine();

        System.out.println("Enter last name");
        this.lastName = FileUtility.getInput().nextLine();

        System.out.println("Enter dob");
        this.dob = FileUtility.getInput().nextLine();

        this.registrationId = ++maxRegistrationId;
    }

    /**
     * Custom constructor with all info
     * @param registrationId
     * @param firstName
     * @param lastName
     * @param dob 
     * 
     * @author BJ MacLean
     * @since 20150917
     */
    public Camper(int registrationId, String firstName, String lastName, String dob) {
        this.registrationId = registrationId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
    }

    
    
    /**
     * constructor which will create from csvString
     *
     * @since 20150917
     * @author BJ MacLean
     */
    public Camper(String[] parts) {
        this(Integer.parseInt(parts[0]),parts[1],parts[2],parts[3]);
        if(Integer.parseInt(parts[0]) > maxRegistrationId){
            maxRegistrationId = Integer.parseInt(parts[0]);
        }
    }

    public String getCSV() {
        return registrationId + "," + firstName + "," + lastName + "," + dob;
    }

    public int getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(int registrationId) {
        this.registrationId = registrationId;
    }

    public static int getMaxRegistrationId() {
        return maxRegistrationId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @Override
    public String toString() {
        return "RegistrationId=" + registrationId + ", firstName=" + firstName + ", lastName=" + lastName + ", dob=" + dob;
    }

    
    
}
